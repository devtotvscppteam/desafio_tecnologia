#DESAFIO TECNOLOGIA
# Redução de String
Dada uma string que contenha as letras, ‘d’, ‘e’, e ‘v’, a seguinte operação deve ser realizada:
- Pegar duas letras adjacentes e distintas e trocar pelo terceiro caractere.
Por exemplo, se tiver um ‘d’ e um ‘e’ lado a lado, elas podem ser trocadas por um ‘v’.
Encontre a menor string que possa ser obtida realizando essa operação repetidamente.

# Formato de Entrada
A primeira linha contém a quantidade de testes T. As linhas subsequentes contém as strings que serão manipuladas.

# Restrições
- A primeira linha da entrada conterá um número T entre 1 e 100;
- A string a ser manipulada terá no máximo 100 caracteres.

# Formato de Saída
A saída terá T linhas, uma para o resultado de cada string manipulada, o resultado consiste no tamanho da menor string resultante após aplicar as operações repetidamente.

# Requisitos:
- Utilizar c++
- Quebrar o processamento entre threads
- Executar em Windows e Linux

# Ganha mais pontos:
- Soluções que performem bem e não consumam muita memória
- Pouca utilização de bibliotecas de terceiros

# Boa Sorte